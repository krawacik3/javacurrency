import org.jfree.data.time.Day;
import org.junit.Assert;

import java.text.ParseException;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class ChartGeneratorTest {

    @org.junit.jupiter.api.Test
    void testParseValidDate(){
        String date = "2020-05-10";
        try {
            Day day = ChartGenerator.stringDateToDay(date);
            Assert.assertEquals(day.getDayOfMonth(), 10);
            Assert.assertEquals(day.getMonth(), 5);
            Assert.assertEquals(day.getYear(), 2020);
        } catch (ParseException e) {
            Assert.fail();
        }
    }

    @org.junit.jupiter.api.Test
    void testParseInvalidDate(){
        String date = "2020-25-50";
        try {
            Day day = ChartGenerator.stringDateToDay(date);
            Assert.assertNotEquals(day.getDayOfMonth(), 50);
            Assert.assertNotEquals(day.getMonth(), 25);
            Assert.assertNotEquals(day.getYear(), 2020);
        } catch (ParseException e) {
            Assert.fail();
        }
    }

    @org.junit.jupiter.api.Test
    void testParseInvalidString(){
        String date = "2020-da-x0";
        try {
            Day day = ChartGenerator.stringDateToDay(date);
            Assert.fail();
        } catch (ParseException e) {
        }
    }
}