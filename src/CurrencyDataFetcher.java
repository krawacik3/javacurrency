import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.json.*;

public class CurrencyDataFetcher {
    static Vector<Vector<CurrencyData>> getCurrencySeries(String base, int numberOfDays, int interval, Vector<String> requestedCurrencies){
        Vector<Vector<CurrencyData>> result = new Vector<Vector<CurrencyData>>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();

        for(int day = 0; day < numberOfDays; day += interval){
            calendar.add(Calendar.DAY_OF_MONTH, -interval);
            Date date = calendar.getTime();
            String formattedDate = format.format(date);
            try {
                // Parse vector to array
                Object[] objectArray = requestedCurrencies.toArray();
                String[] stringArray = Arrays.copyOf(objectArray, objectArray.length, String[].class);

                Vector<CurrencyData> resultFromDay = getCurrency(base, formattedDate, stringArray);
                result.add(resultFromDay);
            }
            catch (IOException ex){
                System.out.print(ex.getMessage());
            }
        }

        return result;
    }

    static Vector<CurrencyData> getCurrency(String base, String date, String[] requestedCurrencies) throws IOException {
        StringBuilder baseAddress = new StringBuilder("https://api.exchangeratesapi.io");
        // Build correct address
        baseAddress.append("/").append(date);
        baseAddress.append("?base=").append(base);
        baseAddress.append("&symbols=");
        for (String requestedCurrency: requestedCurrencies) {
            baseAddress.append(requestedCurrency).append(",");
        }
        // Remove last character of a string, because additional comma was added in loop
        String address = baseAddress.substring(0, baseAddress.length() - 1);
        System.out.println(address);

        // Now request data from built url
        URL url = new URL(address);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        // Variable result contains requested data in JSON format
        String result = bufferedReader.readLine();
        return getCurrencyDataFromJson(result);
    }

    private static Vector<CurrencyData> getCurrencyDataFromJson(String rawJson){
        System.out.println("Parsing JSON:");
        System.out.println(rawJson);
        JSONObject jsonObject = new JSONObject(rawJson);
        String date = jsonObject.get("date").toString();

        Vector<CurrencyData> result = new Vector<CurrencyData>();

        JSONObject rates = (JSONObject) jsonObject.get("rates");
        for(String key: rates.keySet()){
            result.add(new CurrencyData(1. / (Double) rates.get(key), date, key));
        }
        return result;
    }
}
