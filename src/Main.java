import javax.swing.*;
import java.io.IOException;
import java.util.Vector;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello world!");
//        Vector<CurrencyData> values = CurrencyDataFetcher.getCurrency("PLN", "2020-04-20", new String[]{"EUR", "CAD", "AUD"});
//        for(CurrencyData value : values){
//            System.out.println(value.toString());
//        }

        JFrame frame = new JFrame("My fantastic app <3");
        GUI gui = new GUI();
        frame.setContentPane(gui.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
