import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyDataTest {

    @Test
    void parseToStringValid() {
        CurrencyData validCurrency = new CurrencyData(1.0, "10-05-2020", "EUR");
        Assert.assertEquals(validCurrency.toString(), "EUR: 1.0, at 10-05-2020");
    }
}