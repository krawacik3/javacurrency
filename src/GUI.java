import org.jfree.chart.ChartPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class GUI {
    // Main panel used to display all childrens
    public JPanel mainPanel;
    // Panel used by JFreeChart
    private JPanel chartPanel;

    private JComboBox comboBox1;
    private JSpinner spinner1;
    private JButton refreshButton;
    private JCheckBox EURCheckBox;
    private JCheckBox USDCheckBox;
    private JCheckBox GBPCheckBox;
    private JCheckBox PLNCheckBox;
    private JCheckBox CADCheckBox;
    private JComboBox comboBox2;

    public GUI() {
        // Set all default values and behaviors
        SpinnerModel spinnerModel = new SpinnerNumberModel(1, 1, 30, 1);
        spinner1.setModel(spinnerModel);


        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.print("Klikłem");
                updateChart();
            }
        });

    }
    void updateChart(){
        Vector<String> requestedCurrencies = getRequestedCurrencies();
        if(requestedCurrencies.size() == 0){
            return;
        }

        String baseCurrency = comboBox2.getSelectedItem().toString();
        // Make sure to exclude base currency from requested currencies
        requestedCurrencies.removeElement(baseCurrency);
        int interval = getResolution();
        int duration = getDuration();

        // Get values of the requested currencies
        Vector<Vector<CurrencyData>> currencyData = CurrencyDataFetcher.getCurrencySeries(baseCurrency, duration, interval, requestedCurrencies);
        ChartPanel cp = ChartGenerator.generateChartFromData(currencyData, requestedCurrencies);
        chartPanel.removeAll();
        chartPanel.setLayout(new BorderLayout());
        chartPanel.add(cp, BorderLayout.CENTER);
        mainPanel.validate();
        mainPanel.repaint();
    }

    private Vector<String> getRequestedCurrencies(){
        Vector<String> result = new Vector<String>();
        if(EURCheckBox.isSelected()){
            result.add("EUR");
        }
        if(USDCheckBox.isSelected()){
            result.add("USD");
        }
        if(GBPCheckBox.isSelected()){
            result.add("GBP");
        }
        if(PLNCheckBox.isSelected()){
            result.add("PLN");
        }
        if(CADCheckBox.isSelected()){
            result.add("CAD");
        }
        return result;
    }

    private int getDuration(){
        String item = comboBox1.getSelectedItem().toString();
        if(item == "week"){
            return 7;
        }
        if(item == "month"){
            return 31;
        }
        if(item == "quarter"){
            return 91;
        }
        return 365;
    }

    private int getResolution(){
        return (int)spinner1.getValue();
    }
}
