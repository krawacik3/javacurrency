public class CurrencyData {
    double value;
    String time;
    String currencyType;

    public CurrencyData(double value_, String time_, String currencyType_){
        value = value_;
        time = time_;
        currencyType = currencyType_;
    }

    public String toString(){
        return currencyType + ": " + value + ", at " + time;
    }
}
