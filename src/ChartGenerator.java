import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ChartGenerator {
    static ChartPanel generateChartFromData(Vector<Vector<CurrencyData>> currencyData, Vector<String> currencyTypes){
        TimeSeriesCollection dataset = generateDatasetFromData(currencyData, currencyTypes);
        JFreeChart chart = ChartFactory.createTimeSeriesChart("Currency rates", "Date", "Rate", dataset, true, true, false);
        ChartPanel cp = new ChartPanel(chart);
        for(var marker : getValueMarkers(currencyData, currencyTypes)){
            cp.getChart().getXYPlot().addRangeMarker(marker);
        }
        return cp;
    }

    private static TimeSeriesCollection generateDatasetFromData(Vector<Vector<CurrencyData>> currencyData, Vector<String> currencyTypes){
        Vector<TimeSeries> currencySeries = new Vector<TimeSeries>();
        for(int x = 0; x < currencyTypes.size(); ++x){
            currencySeries.add(new TimeSeries(currencyTypes.elementAt(x)));
        }

        for(var currenciesAtDate : currencyData){
            try {
                Day date = stringDateToDay(currenciesAtDate.firstElement().time);
                for (var currency : currenciesAtDate) {
                    // Position in currencyTypes is also position in currencySeries
                    int index = currencyTypes.indexOf(currency.currencyType);
                    double yValue = currency.value;
                    currencySeries.elementAt(index).addOrUpdate(date, yValue);
                }
            }
            catch (ParseException ex){
                System.out.print(ex.toString());
            }
        }

        // Add all series to a collection
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        for(var series : currencySeries){
            dataset.addSeries(series);
        }
        return dataset;
    }

    static Day stringDateToDay(String rawDate) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(rawDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH ) + 1;
        int year = calendar.get(Calendar.YEAR);
        return new Day(day, month, year);
    }

    static Vector<ValueMarker> getValueMarkers(Vector<Vector<CurrencyData>> currencyData, Vector<String> currencyTypes) {
        Vector<Double> minValues = new Vector<Double>();
        Vector<Double> maxValues = new Vector<Double>();
        // Initialize vectors with dummy values
        for (int index = 0; index < currencyTypes.size(); ++index) {
            minValues.add(Double.MAX_VALUE);
            maxValues.add(0.);
        }

        for (var currenciesAtDate : currencyData) {
            for (var currency : currenciesAtDate) {
                int index = currencyTypes.indexOf(currency.currencyType);
                if (currency.value > maxValues.get(index)) {
                    maxValues.set(index, currency.value);
                }
                if (currency.value < minValues.get(index)) {
                    minValues.set(index, currency.value);
                }
            }
        }

        // Create all ValueMarkers and add them to the vector
        Vector<ValueMarker> valueMarkers = new Vector<ValueMarker>();
        for (int index = 0; index < currencyTypes.size(); ++index) {
            // Min
            ValueMarker minValueMarker = new ValueMarker(minValues.get(index));
            minValueMarker.setPaint(Color.magenta);
            minValueMarker.setStroke(new BasicStroke(2));
            minValueMarker.setLabel("Min: " + minValues.get(index).toString());
            minValueMarker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
            minValueMarker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
            valueMarkers.add(minValueMarker);

            // Max
            ValueMarker maxValueMarker = new ValueMarker(maxValues.get(index));
            maxValueMarker.setPaint(Color.cyan);
            maxValueMarker.setStroke(new BasicStroke(2));
            maxValueMarker.setLabel("Max: " + maxValues.get(index).toString());
            maxValueMarker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
            maxValueMarker.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);
            valueMarkers.add(maxValueMarker);
        }

        return valueMarkers;
    }
}
